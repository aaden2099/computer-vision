import cv2
import time


# Load haarcascade classifiers for frontal face detection:
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# Create VideoCapture object to get images from the camera
video_capture = cv2.VideoCapture(0)

# To use a video file (located in the same folder) instead uncomment the line below
#video_capture = cv2.VideoCapture('filename.mp4')

# Allow Webcam to warm up
time.sleep(2.0)


while True:
    # Capture frame from the VideoCapture object:
    ret, frame = video_capture.read()

    # Convert frame to grayscale:
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect faces within the frame
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    # Draw the rectangle around each detected face
    for (x, y, w, h) in faces:
        if h > 0 and w > 0:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
        
    # Display the resulting frame with title
    cv2.imshow('Detect Faces', frame)
        
    # Press q (lowercase Q) to stop the program
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


# Release everything when program ends:
video_capture.release()
cv2.destroyAllWindows()
