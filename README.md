# Face Detector

An OpenCV project, written in Python 3, which uses Haar Cascades to detect faces, and places a bounding box around each detected face.

# Installing Python 3

If you don't have Python 3 installed on your system, click the following link to download the latest stable release: https://www.python.org/downloads/


# Installing pip

pip is the reference Python package manager. It’s used to install and update packages. You’ll need to make sure you have the latest version of pip installed.

### Windows

The Python installers for Windows include pip. To see which version of pip you have as well as the location where it was installed, execute the following command in your terminal.

```
py -m pip --version`
pip 20.0.2 from C:\Python37\lib\site-packages (Python 3.7)
```

You can make sure that pip is up-to-date by running:

`py -m pip install --upgrade pip`


### Linux and Mac

Debian and most other linux distributions as well as Mac OS's include a python-pip package. However, you can also install pip yourself to ensure you have the latest version. It’s recommended to use the system pip to bootstrap a user installation of pip:

`python3 -m pip install --user --upgrade pip`

Afterwards, you should have the newest pip installed in your user site:

```
python3 -m pip --version
pip 9.0.1 from $HOME/.local/lib/python3.6/site-packages (python 3.6)
```


# venv

The venv module is the preferred way to create and manage virtual environments. For Python 3.3 or newer, venv is included in the Python standard library and requires no additional installation. To create a virtual environment, navigate to the project directory and run venv. 

### On macOS and Linux:

```python3 -m venv env```


### On Windows:

```py -m venv env```


The second argument is the location to create the virtual environment. Generally, you can just create this in your project and call it env or venv.


# Activating a virtual environment

Before you can start installing or using packages in your virtual environment you’ll need to activate it. Activating a virtual environment will put the virtual environment-specific python and pip executables into your shell’s PATH.

### On macOS and Linux:

`source env/bin/activate`

### On Windows:

`.\env\Scripts\activate`


You can confirm you’re in the virtual environment by checking the location of your Python interpreter, it should point to the env directory.


### On macOS and Linux:

```
which python
.../env/bin/python
```


### On Windows:

```
where python
.../env/bin/python.exe
```

As long as your virtual environment is activated pip will install packages into that specific environment and you’ll be able to import and use packages in your Python application.


# Leaving a virtual environment

If you want to switch projects or otherwise leave your virtual environment, simply run:

`deactivate`


If you want to re-enter the virtual environment just follow the same instructions above about activating a virtual environment. There’s no need to re-create the virtual environment.


# Installing project requirements.

Navigate to the project's main directory where the requirements.txt file is located and execute the following command in your terminal:

`pip3 install -r requirements.txt`

or, if you only have Python3 installed on your machine:

`pip install -r requirements.txt`


If you want to re-enter the virtual environment just follow the same instructions above about activating a virtual environment. There’s no need to re-create the virtual environment.


# Running the program

Navigate to the directory of the project that you wish to run within your terminal window and execute the following command:

`python3 detect_faces.py`

If Python 3 is the only Python interpreter that's installed on your system, then you can run the following command to start the program:

`python detect_faces.py`